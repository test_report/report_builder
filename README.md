# ReportBuilder

Приложение для построени отчетов по продажам в разрезе продуктов и брэндов.

Для запуска отчета необходимо выполнить следующие шаги:
1. Установить Apache Spark >= 2.4.7
2. Запустить команду `sh report/build_report <KEY>=<VALUE>`, где `<KEY>=<VALUE>` - список "ключ-зачение" параметров, передаваемых в отчет.

<h2 id="dataflow.io/EndpointsSpecification">Параметры отчета
</h2>
<table>
<thead>
<tr>
<th>Параметр</th>
<th>Описание</th>
<th>Обязательный?</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<code>PRODUCT_NAMES_FILE_PATH</code>
</td>
<td>
Путь к файлу product_names.csv. По умолчанию - product_names.csv
</td>
<td>
Небязательный
</td>
</tr>
<tr>
<td>
<code>DATE_FROM</code>
</td>
<td>
Дата начала отчетного периода
</td>
<td>
Обязательный
</td>
</tr>
<tr>
<td>
<code>DATE_TO</code>
</td>
<td>
Дата окончания отчетного периода
</td>
<td>
Обязательный
</td>
</tr>
<tr>
<td>
<code>KKT_CATEGORIES</code>
</td>
<td>
Список категорий ККТ
</td>
<td>
Необязательный
</td>
</tr>
<tr>
<td>
<code>RECEIPT_DATE</code>
</td>
<td>
Признак группировки по дате продажи. По умолчанию - false
</td>
<td>
Необязательный
</td>
</tr>
<tr>
<td>
<code>REGION</code>
</td>
<td>
Признак группировки по региону. По умолчанию - false
</td>
<td>
Необязательный
</td>
</tr>
<tr>
<td>
<code>CHANNEL</code>
</td>
<td>
Признак группировки по полю channel. По умолчанию - false
</td>
<td>
Необязательный
</td>
</tr>
</tbody>
</table>

Пример:
```
sh report/build_report.sh DATE_FROM=2019-08-01 DATE_TO=2019-08-31 RECEIPT_DATE=false REGION=true CHANNEL=false KKT_CATEGORIES=FMCG
```

3. Результаты отчета сохраняются в файл `report/report`




Примeчание: 
Отчеты, построенные по п. 3 задания доступны в папке report:
<h2 id="dataflow.io/EndpointsSpecification">Параметры отчета
</h2>
<table>
<thead>
<tr>
<th>Файл</th>
<th>date_from</th>
<th>date_to</th>
<th>categories</th>
<th>receipt_date</th>
<th>region</th>
<th>channel</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<code>report/report1</code>
</td>
<td>
2019-08-01
</td>
<td>
2019-08-31
</td>
<td>
</td>
<td>false
</td>
<td>false
</td>
<td>false
</td>
</tr>
<tr>
<td>
<code>report/report2</code>
</td>
<td>
2019-08-01
</td>
<td>
2019-08-31
</td>
<td>
</td>
<td>true
</td>
<td>true
</td>
<td>true
</td>
</tr>
<tr>
<td>
<code>report/report3</code>
</td>
<td>
2019-08-05
</td>
<td>
2019-08-11
</td>
<td>
FMCG
</td>
<td>false
</td>
<td>true
</td>
<td>false
</td>
</tr>
</tbody>
</table>


