package org.test.spark.builder

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import scala.reflect.io.File


/**
 *
 * Приложение для построения отчетов по продажам в разрезе продуктов и брэндов.
 *
 * @author Sergey Samsonov <a href="mailto:ssamsonov.alpha@gmail.com">ssamsonov.alpha@gmail.com</a>
 *
 * @since 2022-11-30
 *
 * Аргументы:
 * args(0) - JDBC URL для подключеи як БД. Обязательный аргумент
 * args(1) - Название класса JDBC драйвера для подключения к БД. Обязательный аргумент
 * args(2) - Путь к файлу product_names.csv. Обязательный аргумент
 * args(3) - Дата начала отчетного периода. Обязательный аргумент
 * args(4) - Дата окончания отчетного периода. Обязательный аргумент
 * args(5) - Признак группировки по дате продажи. Обязательный аргумент
 * args(6) - Признак группировки по региону. Обязательный аргумент
 * args(7) - Признак группировки по полю channel. Обязательный аргумент
 * args(8) - Признак группировки по региону. Обязательный аргумент
 * args(9) - Список категорий ККТ. Необязательный аргумент
 *
 */

object ReportBuilder extends App with LazyLogging {

  final val APPLICATION_NAME = "ReportBuilder"

  final val CSV_REPORTS_DELIMITER = ","

  final val OUTPUT_FOLDER_NAME = "output"

  override def main(args: Array[String]): Unit = {

    lazy val ARGUMENTS = Map(
      "0" -> ("JDBC URL to connect to database","mandatory"),
      "1" -> ("JDBC driver to connect to database","mandatory"),
      "2" -> ("Path to file containing mappings between products and brands","mandatory"),
      "3" -> ("Start date of the reporting period","mandatory"),
      "4" -> ("End date of the reporting period","mandatory"),
      "5" -> ("Flag, whether the data needed to be grouped by receipt_date","mandatory"),
      "6" -> ("Flag, whether the data needed to be grouped by region","mandatory"),
      "7" -> ("Flag, whether the data needed to be grouped by channel","mandatory"),
      "8" -> ("The list of KKT categories to apply for the report","optional")
    )

    // Проверяем, что обязательные аргументы проинициализированы
    if (args.length < 8) {

      logger.error(s"Error: The mandatory options are not provided. Please specify the following arguments: " +
        ARGUMENTS)

      System.exit(1)

    }

    val salesViewDbUrl = args(0).toString

    val salesViewDbDriverClass = args(1).toString

    val brandsFilePath = args(2).toString

    val dateFrom = args(3).toString

    val dateTo = args(4).toString

    val receipt_date = args(5).toString

    val region = args(6).toString

    val channel = args(7).toString

    val kktCategories = if (args.length < 9 ) ""
      else s"'${args(8).toString.replaceAll(",", "','")}'"

    // Проверяем, что файл product_names.csv существует по указанному в аргументах пути
    if (!File(brandsFilePath).exists) {

      logger.error(s"Error: File ${brandsFilePath} not found. Please specify the correct path to the file")

      System.exit(1)

    }

    val sparkSession = SparkSession.builder().appName(APPLICATION_NAME).getOrCreate()

    var orgViewSelectFields = "ki.org_inn"

    var orgViewFields = "org_inn"

    var salesViewFields = "product_name_hash, total_sum as tot_sum, org_inn"

    var joinedDfGroupByKeys = "brand"

    // Определяем набор полей-ключей групировки
    if (receipt_date.equalsIgnoreCase("true")) {
      salesViewFields = salesViewFields + ", receipt_date"
      joinedDfGroupByKeys = joinedDfGroupByKeys + ",receipt_date"
    }

    if (region.equalsIgnoreCase("true")) {
      orgViewSelectFields = orgViewSelectFields + ", region"
      orgViewFields = orgViewFields + ", region"
      joinedDfGroupByKeys = joinedDfGroupByKeys + ",region"
    }


    if (channel.equalsIgnoreCase("true")) {
      orgViewSelectFields = orgViewSelectFields + ", shop_id, region"
      orgViewFields = orgViewFields + ", channel"
      joinedDfGroupByKeys = joinedDfGroupByKeys + ",channel"
    }

    // Формируем запрос для выгрузки данных из таблицы продаж
    val salesQuery = s"SELECT ${salesViewFields} FROM sales WHERE DATE(receipt_date) >= DATE('${dateFrom}') AND " +
      s"DATE(receipt_date) <= DATE('${dateTo}') "

    // Выгружаем данные из таблицы продаж
    val salesDf = sparkSession.read.format("jdbc")
      .option("url",salesViewDbUrl)
      .option("driver",salesViewDbDriverClass)
      .option("dbtable",s"(${salesQuery})").load().as("sales")

    var joinOrgViewSt = ""

    if (!kktCategories.isEmpty()|| region.equalsIgnoreCase("true")
      || channel.equalsIgnoreCase("true")) {
      var orgViewQuery = s"SELECT ${orgViewSelectFields} FROM kkt_info ki "

      // Если указан фильтр по категориям, проводим отбор организаций по активным категориям в соответствии с фильтром
      if (!kktCategories.isEmpty()) {
        orgViewQuery = s"${orgViewQuery} INNER JOIN (select * from (select category, kkt_number, date_from, " +
          s"date_till, version, MAX(version) OVER(PARTITION BY category) as max_version FROM kkt_categories WHERE " +
          s"category IN (${kktCategories})) WHERE version = max_version AND DATE(date_from) <= DATE('${dateTo}') " +
          s"AND DATE(date_till) >= DATE('${dateFrom}')) kc ON ki.kkt_number = kc.kkt_number "
      }

      // Если указан признак граппировки по пполу channel, проводим отбор организаций по активностям, соответствующим
      // отчетному периоду
      if (channel.equalsIgnoreCase("true")) {
        orgViewQuery = s"${orgViewQuery} INNER JOIN (SELECT * FROM kkt_activity WHERE DATE(receipt_date_min) " +
          s"<= DATE('${dateTo}') AND DATE(receipt_date_max) >= DATE('${dateFrom}')) kact ON " +
          "ki.kkt_number = kact.kkt_number "
      }

      // Формируем запрос для построения представления по организациям
      orgViewQuery = orgViewQuery + " WHERE ki.org_inn in (SELECT distinct org_inn FROM sales WHERE " +
        s" DATE(receipt_date) >= DATE('${dateFrom}') AND DATE(receipt_date) <= DATE('${dateTo}')) AND " +
        s" DATE(ki.date_from) <= DATE('${dateTo}') AND DATE(ki.date_till) >= DATE('${dateFrom}') "

      // Выполняем потроение представления по организациям и сохраняем в датафрэйм
      var orgViewDf = sparkSession.read.format("jdbc")
        .option("url",salesViewDbUrl)
        .option("driver",salesViewDbDriverClass)
        .option("dbtable",s"(${orgViewQuery})").load().as("org_view")

      //  Если указан признак группировки по полю channel, определяем значение этого и добавляем в датафрэйм
      if (channel.equalsIgnoreCase("true")) {
        orgViewDf = orgViewDf
          .withColumn("channel",
            expr("CASE WHEN COUNT(shop_id) OVER (PARTITION BY org_inn) >= 3 THEN 'chain' ELSE 'nonchain' END"))
      }

      orgViewDf.createOrReplaceTempView("org_view")

      // Выбиряем неповторяющиеся значения полей из представения организаций
      joinOrgViewSt = s" INNER JOIN (SELECT DISTINCT ${orgViewFields} FROM org_view) o ON s.org_inn = o.org_inn "
    }

    // Загружаем файл product_names.csv
    val brandsDf = sparkSession.read.option("header","true").option("delimiter", CSV_REPORTS_DELIMITER)
      .csv(brandsFilePath).as("brands")

    // Выполяем джойн таблиц продаж и брэндов
    var salesAndBrandsDf = salesDf
      .join(brandsDf,col("sales.product_name_hash")
        === col("brands.product_name_hash"), "inner")

    salesAndBrandsDf.createOrReplaceTempView("sales_brands_view")

    val totalAmount = salesAndBrandsDf.agg(sum("tot_sum").cast("long")).first.getLong(0)

    // Формируем запрос для построения отчета
    val outputQuery = s"SELECT ${joinedDfGroupByKeys}, ROUND(SUM(tot_sum),2) as total_sum, ROUND((SUM(tot_sum) / " +
      s"${totalAmount}) * 100,2) AS total_sum_pct FROM sales_brands_view s ${joinOrgViewSt} GROUP BY " +
      s"${joinedDfGroupByKeys} ORDER BY ${joinedDfGroupByKeys}"

    // Строим отчет и выводим в диреторию output
    sparkSession.sqlContext.sql(outputQuery)
      .write.option("header","true").option("delimiter", CSV_REPORTS_DELIMITER).csv(OUTPUT_FOLDER_NAME)

  }

}
