#!/bin/sh

REPORT_FILE_NAME=report
PRODUCT_NAMES_FILE_PATH=product_names.csv
DATE_FROM=
DATE_TO=
KKT_CATEGORIES=
RECEIPT_DATE=false
REGION=false
CHANNEL=false

for ARGUMENT in "$@"
do
  KEY=$(echo $ARGUMENT | cut -f1 -d=)

  KEY_LENGTH=${#KEY}
  VALUE="${ARGUMENT:$KEY_LENGTH+1}"

  export "$KEY"="$VALUE"
done

echo "==================================="
echo "The report running with parameters:"
echo "PRODUCT_NAMES_FILE_PATH = $PRODUCT_NAMES_FILE_PATH"
echo "DATE_FROM = $DATE_FROM"
echo "DATE_TO = $DATE_TO"
echo "KKT_CATEGORIES = $KKT_CATEGORIES"
echo "RECEIPT_DATE = $RECEIPT_DATE"
echo "REGION = $REGION"
echo "CHANNEL = $CHANNEL"
echo "==================================="

spark-submit \
--master local \
--jars /Users/sergey/Documents/sqlite-tools-osx-x86-3400000/sqlite-jdbc-3.34.0.jar \
--class org.test.spark.builder.ReportBuilder \
reports-builder-1.0-SNAPSHOT.jar \
jdbc:sqlite:/Users/sergey/Documents/sqlite-tools-osx-x86-3400000/de_test.db \
org.sqlite.JDBC \
$PRODUCT_NAMES_FILE_PATH \
$DATE_FROM \
$DATE_TO \
$RECEIPT_DATE \
$REGION \
$CHANNEL \
$KKT_CATEGORIES

mv output/part* $REPORT_FILE_NAME

rm -fr output
